# Assignment 02
Tanner Bobak

CSCI 4576

9/12/18

--- 

## Task 1

### Problem 1
To implement the parallel histogram generation algorithm, the first step is to find the minimum and maximum of the vector of data. To do this, a parallel for loop with OpenMP reductions for the std::max and std::min will be created. Using the syntax for a reduction, the singular, scalar values for minimum and maximum can be determined in parallel without encountering race conditions or undefined behavior.

Once the minimum and maximum is found, the range of the data can be calculated. This range will be divided by the number of bins to evaluate the width of each bin. Knowing this, the bin boundaries vector can be populated. The first and last boundary will be assigned in serial because they will be relatively quick operations, and allow them to be independently adjusted such that all data falls within the boundaries. Then, in a standard parallel for loop, the boundaries can be populated with values equal to the bin number times the increment determined from the range of data.

Finally, the data can be assigned to a bin by iterating through the data set in parallel. The bin each data point belongs to can be determined by multiplying the data point by the reciprocal of the bin incrememnt, then truncating the digits right of the decimal point with a cast. These operations combined are somewhat expensive, but they are overall cheaper than a division within a loop. A number of other small optimizations can be applied to this step in order to speed it up such as loop unrolling and careful manipulation of where bin counts are updated in memory. Notably, if each thread writes to its own `bin_count` vector, which will be combined with the other threads later, then the usage of the OpenMP directive `#pragma omp atomic` to stop race conditions can be avoided all together. 

### Problem 2
To evaluate the performance of this code, five different metrics can be examined: algorithmic complexity, total work, execution time, speed-up, and efficiency. 

First, complexity is examined. The algorithm described above to find the minimum and maximum is O(n) because every element in the data array `x` must be iterated through. Bin boundary population is also O(n) because a loop must iterate from zero to the number of bins requested. Finally, the population of each bin with the count of data points within it is an O(n) algorithm. The algorithm is O(n) because all elements of the array must be iterated through once. 

Next, work for each of the three parts of the algorithm is evaluated. Finding the minimum and maximum requires 2n operations without reductions, where an operation is `std::min` or `std::max`. With reductions, the work required is W(n) = n + W(n/2), like the recursive sum example from class. Populating the bin boundaries requires n operations, where n is the number of bins and an operation is one multiplication. The `bin_count` population algorithm's work will in general be 3n + C, because for each data point a multiplication, integer cast, and increment to the bin count must be performed. The '+C' will depend on the specific implementation, such as writing to different array and combining later, and may scale with another smaller in magnitude variable. 

## Task 3

### Problem 1

#### 1a

![dag](dag_300x.jpg)

The work to find x^n where n is 2^k for some integer k and scalar x is equal to k because there are k internal nodes on the DAG (above) of the algorithm. The depth of this algorithm is k since there are k edges between the output and the leaves.

#### 1b

![matrix dag](mdag_300x.jpg)

The work to find x^n where n is 2^k for some integer k and m by m array x is equal to k(2m^3 - m^2) because evaluating the product of two arrays requires 2m^3 - m^2 operations. Every unit of k added requires another 2m^3 - m^2 operations to be conducted. The depth of the algorithm is k+2 because the depth of squaring an array A to obtain A^2 is 3 and subsequent powers of two of the array (A^4, A^8, etc.) require k-1 more edges. 

### Problem 2
A PRAM algorithm for a reduced parallel sum is based on Figure 3 from the class notes "Principles of Parallel Algorithms" and from algorithm 1.2 on page 24 of "Introduction to Parallel Algorithms, 1st ed.". It is a concurrent read, exclusive write algorithm that first copies the array to sum, `x`, into a new array, `y`. Then, assuming there are a power of two elements in x, the algorithm iterates from 1 to log2(n). That is, it iterates through the levels of a binary tree representing sum operations (like Figure 3). Each processor then sums the elements in `y` corresponding to `2i-1` and `2i` where `i` is the thread number, saving the result into `y[i]` with an exclusive write. The algorithm then moves up a level in the tree, and the number of processors doing work is halved because only half the number of partial sums as the previous step are needed. The sum reading and writing tasks are performed again and again until the top level of the tree is reached and the full sum is stored in the first element of `y`. 

In the case where the length of `x` is not evenly divisible by the number of processors `p`, then some processors will not have work on the first iteration. Also, for `p` < `n`, the sums of sub-trees will need to be calculated first in the same fashion until the number of partial sums equals `p`. 

The work for this algorithm is order `n`, and the depth is `1+log2(n)`. Therefore, the parallel execution time is bounded above by `floor(n/p)+log2(n)+1`. This means that the sequential execution time is expected to be order `n`, and so the speedup will be `n/(n/p + log2(n) + 1)` and the corresponding efficiency `n/(n+p*log2(p)+p)`.

### Problem 3
Given a parallel algorithm with serial time `n` and parallel time n/p + log2(p), the efficiency is n/(n + p\*log2(p)). If p is increased by k, then the efficiency becomes n/(n + kp\*log2(kp)). To maintain constant efficiency, the problem size must then be scaled by a factor of (k\*log2(kp))/log2(p). Since a problem size that maintains constant efficiency can be found for arbitrary k, this algorithm is parallel scalable. 

### Problem 4
This loop carried dependence can be eliminated by noting that this algorithm is storing the sum of the numbers from zero to n at location n in array `a`. That is, the array `a` looks like `a = {0, 1, 3, 6, 10, 15, ...}`. It can then be noted that the sum of the natural numbers to n is equal to n(n-1)/2. So, this loop can be parallelized by substituting in this formula, like so:

```
a[0] = 0;
for (i = 1; i < n; ++i)
	a[i] = i*(i+1)/2; // Note n = i+1 because indexing starts at 0
```

Or, better yet,

```
a[0] = 0;
for (i = 1; i < n; ++i)
	a[i] = (i*(i+1)) >> 1; // Bit shifts are much faster than division.
```