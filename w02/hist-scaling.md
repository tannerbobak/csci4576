# Assignment 02
Tanner Bobak

CSCI 4576

9/12/18

--- 

## Task 2

The following plots show wall clock time and speedup for my implementation on a machine with an AMD Ryzen 7 1800X processor, which has 8 physical cores. The machine is running Ubuntu 18.04.

It can be seen that this implementation is best for very large workloads, where the speedup is almost a factor of 7. For small workloads, the parallel implementation is actually far worse. 

It should also be noted that the serial verison can be compiled with `make serial` and the parallel version with `make parallel`.

Finally, it is noteworthy that certain compiler flags, `-O3` and `-funroll-loops`, can make the serial version faster on my machine in almost any case. The plots below are shown without these flags. However, to see the performance of the algorithm with these flags, one can run `make warpspeed` and then run `./engage.sh <number of threads>`.

![plot1](10binwall.png)
![plot2](1000binwall.png)
![plot3](10000binwall.png)
![plot4](10binspeed.png)
![plot5](1000binspeed.png)
![plot6](10000binspeed.png)