export OMP_NUM_THREADS=${1}

./test_histogram.exe 2 1
./test_histogram.exe 2 3
./test_histogram.exe 2 4

./test_histogram.exe 5 1
./test_histogram.exe 5 3
./test_histogram.exe 5 4

./test_histogram.exe 9 1
./test_histogram.exe 9 3
./test_histogram.exe 9 4