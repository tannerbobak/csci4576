/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 1 $
 * @tags $Tags: tip $
 * @date $Date: Tue Sep 26 18:22:46 2017 -0600 $
 */

/*
 * @input x data vector
 * @input num_bins the number of bins to use
 * @output bin_bdry vector of size num_bins+1 holding the left and
 *         right limit of each bin
 * @output bin_count array of size num_bins holding the count of
 *         items in each bin.
 *
 * Both output vectors are cleared and filled out by the histogram
 * function.
 */

#include <cstddef>
#include <vector>

typedef std::vector<double> vec_t;
typedef std::vector<size_t> count_t;

void histogram(const vec_t &x, int num_bins,          /* inputs  */
	           vec_t &bin_bdry, count_t &bin_count);  /* outputs */