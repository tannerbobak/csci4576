/*
 * @input x data vector
 * @input num_bins the number of bins to use
 * @output bin_bdry vector of size num_bins+1 holding the left and
 *         right limit of each bin
 * @output bin_count array of size num_bins holding the count of
 *         items in each bin.
 *
 * Both output vectors are cleared and filled out by the histogram
 * function.
 */

#include <cstddef>
#include <vector>
#include <cstdio>
#include <cfloat>
#include "omp-utils.h"

typedef std::vector<double> vec_t;
typedef std::vector<size_t> count_t;

#ifdef _OPENMP
void histogram(const vec_t &x, int num_bins,           /* inputs  */
	           vec_t &bin_bdry, count_t &bin_count) {  /* outputs */

	// Create vectors
	bin_bdry.reserve(num_bins+1);
	bin_count.reserve(num_bins);

	// Number of threads
	int nt = omp_get_max_threads();

	// Size of vector
	int sz = x.size();

	// Find minimum and maximum.
	double my_min = DBL_MAX;
	double my_max = DBL_MIN;
	double range = 0;

	// Compute the minimum and maximum
	#pragma omp parallel for reduction(min:my_min) reduction(max:my_max)
	for(int i = 0; i < sz; ++i) {
		my_min = std::min(x[i], my_min);
		my_max = std::max(x[i], my_max);
	}

	// Compute the range of values
	range = my_max-my_min;

	// Compute the width of each bin.
	double inc = range/num_bins;

	// Compute a scale factor used to sort each item into the correct bin
	// without needing to resort to division.
	double s = 1/inc;

	// Adjust the upper and lower bound so all elements are compared correctly. 
	bin_bdry[0] = my_min*(1-1E-14);
	bin_bdry[num_bins] = my_max*(1+1E-14);

	// Populate boundaries
	#pragma omp parallel for 
	for(int i = 1; i < num_bins; ++i) {
		bin_bdry[i] = i*inc;
	}

	#pragma omp parallel
	{
		// Create a local count vector
		count_t thr_count;
		thr_count.reserve(num_bins);

		// Populate counts into each thread
		#pragma omp for
		for(int i = 0; i < sz; ++i) {
			int bin = (int) s*x[i];

			// Handle outlier
			if(bin == num_bins) bin = num_bins - 1;

			thr_count[bin] += 1;
		}

		#pragma omp critical
		{
			for(int i = 0; i < num_bins; ++i) {
				bin_count[i] += thr_count[i];
			}
		}
	}

}

#else
void histogram(const vec_t &x, int num_bins,           /* inputs  */
	           vec_t &bin_bdry, count_t &bin_count) {  /* outputs */

	// Create vectors by allocating the memory for them.
	bin_bdry.reserve(num_bins+1);
	bin_count.reserve(num_bins);

	// Size of vector
	int sz = x.size();

	// Find minimum and maximum, using the known fact that values are
	// are distributed between 0 and 1.
	double my_min = 1;
	double my_max = 0;
	double range = 0;

	int i;
	double v;
	for(i = 0; i < sz; ++i) {
		v = x[i];
		my_min = std::min(my_min, v);
		my_max = std::max(my_max, v);
	}

	// Compute the range of values
	range = my_max-my_min;

	// Compute the width of each bin.
	double inc = range/num_bins;

	// Compute a scale factor used to sort each item into the correct bin
	// without needing to resort to division.
	double s = 1/inc;

	// Adjust the upper and lower bound so all elements are compared correctly. 
	bin_bdry[0] = my_min*(1-1E-14);
	bin_bdry[num_bins] = my_max*(1+1E-14);

	int bin1, bin2;
	
	// Populate boundaries
	for(i = 1; i < num_bins; ++i) {
		bin_bdry[i] = i*inc;
	}
	
	// Populate counts, loop unroll
	for(i = 0; i < sz; i+=2) {
		bin1 = (int) s*x[i];
		bin2 = (int) s*x[i+1];

		bin_count[bin1] += 1;
		bin_count[bin2] += 1;
	}
	
}
#endif