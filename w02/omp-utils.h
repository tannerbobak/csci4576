/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 1 $
 * @tags $Tags: tip $
 * @date $Date: Tue Sep 26 18:22:46 2017 -0600 $
 *
 * @brief openmp utility functions
 */

#ifndef _OMP_UTILS_H_
#define _OMP_UTILS_H_

#ifdef _OPENMP
#include <omp.h>
#define NOW()(omp_get_wtime())
#else
#include <time.h>
#define NOW()(((float)clock())/CLOCKS_PER_SEC)
#define omp_get_thread_num() 0
#define omp_get_max_threads() 1
#endif

#endif //_OMP_UTILS_H_
