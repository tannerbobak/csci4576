export OMP_NUM_THREADS=${1}

./warp.exe 2 1 0
./warp.exe 2 3 0
./warp.exe 2 4 0

./warp.exe 5 1 0
./warp.exe 5 3 0
./warp.exe 5 4 0

./warp.exe 9 1 0
./warp.exe 9 3 0
./warp.exe 9 4 0