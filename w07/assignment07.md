# Assignment 07
Tanner Bobak

CSCI 4576

October 26th, 2018

---

## Task 1

Unfortunately, due to life events outside of coursework, I was unable to complete a working implementation of the Poisson solver in time for the due date of this assignment, despite the extension. During the two weeks leading up to this assignment's due date I was frantically preparing for an internation competition out-of-state that required extensive engineering. This, among the coursework for my other classes as well as a job left me without the free blocks of time needed to sit down and program this Poisson solver implementation.

However, I am able to describe in detail my plan for the implementation of the solver. I hope that this will be able to help demonstrate my understanding of the implementation of network topologies, MPI, scaling, and performance analysis.

First, the `poisson_setup` function is called from `test_poisson.cc` and initializes MPI. Once initialized, the number of processors is fectched and the rank of the current processor is determined. Subsequently, a 3D cartesian grid topology is initialized in using MPI's `MPI_Dims_create` and `MPI_Cart_create` functions. First, an array representing the dimensions (edge lengths, per se) of the cube is created based on the number of available processors and the three dimensions that are required. To explain, if there are 27 processors available then the problem will be set up in a three by three by three grid. Similarly, if 12 processors are available, then the processors would be arranged in a two by three by two grid.

After determining the layout of the processors, each processor is assigned points to evaulate using its location within the grid. Each unit volume assigned to a processor has a number of points associated with it depending on the grid layout determined previously. The coordinates of the processor are used to specify exactly which points belong to each processor. The points populate a vector `x` for each processor. This concludes the setup for the residual solver.

After the MPI environment is setup, the LaPlacian operator must be applied to the candidate solution, which is handled in the function `poisson_matvec`. This presents several problems. First, the discretization points for each processor are stored as a vector as opposed to a multidimentional array. As a consequence, some math must be conducted to find the indices of the points adjacent to the point currently being evaluated. Further complicating the issue is the fact that discretization points on the edge of rely on the values of the function at dicretization points not belonging to the current processor, or that are beyond the bounds of the problem. To get around this, processing nodes have to communicate values to each other. To determine which processor needs to be communicated with, the function `MPI_Cart_shift` is used. Then, the functions `MPI_send` and `MPI_recv` can be used to communicate values.

With all the values that the function needs to be evaluated at on hand, the LaPlacian of the function `u(x)` can be evaluated and subtracted from the constant `a` times `u(x)`. This value is then set into the vector `lv` and returned from `poisson_matvec`. 

Finally, the resisdual can be calculated using the given equation in the assignment description. In this equation, the value of the discrete numerical approximation of the solution is subtracted from the known analytical solution to obtain a residual value at each point. Then, the L-2 norm of the residuals can be computed to obtain a scalar estimate for the accuracy of this numerical method. The expected result is that as more discretization points are used, the norm of the residuals decreases and the numerical method more and more closely approximates the actual solution.

---

The next part of the assignment was to perform strong and weak scaling analysis on the code written using the given functions `a(x)`, `u(x)`, and `f(x)`. As I do not have working implementation, I will describe how I would have went about testing the scaling. 

I would have used code similar to that in the previous assignment to run the program multiple times and with different numbers of processors (strong scaling) and different problem sizes (weak scaling). Then I would compile the timing data into a plot. If the implementation scales well, I would expect to see that speedup increases with an increasing number of processors, but at a slowing rate as parallel overhead increases. Also, if the problem size per processor remains constant (e.g. 2x more processors means 2x more total work), then a highly scalable problem should see a relatively constant speedup as the number of processors increases, implying that the program does not have much extra parallel overhead. 

This is how I would have finished my implentation of this week's assignment. I wish I could have finished as it is an interesting problem with real-world application (I have used a Poisson-Boltzmann solver professionally in research), but I simply ran out of time to create a working implementation with the rest of life happening. I hope that this report describes how I would have approached the problem in adequete detail to demonstrate my understanding of the material.