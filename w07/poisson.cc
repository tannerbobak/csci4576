/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */

#include "poisson.h"
#include <iostream>

void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{
    std::cout<<"setup"<<std::endl;
	
	int p; // Number of processors.
	int i; // Specific processor ID.
	int ndims = 3;
	
	// Initialize
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &i);
	
	// Create cartesian grid based on number of processors and dimensions.
	int dims[ndims];
	MPI_Dims_create(p, ndims, dims);
	bool per[ndims];
	for(int cnt = 0; cnt < ndims; ++cnt) per[cnt] = false;
	MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, per, false, &grid_comm);
	
	// Get the coordinate of the current processor, populate its vector of grid points.
	idx = 0;
	int coords[3]; // [0] is a, [1] is b, [2] is c
	int idim = n/coords[0];
	int jdim = n/coords[1];
	int kdim = n/coords[2];
	MPI_cart_coords(grid_comm, i, 3, coords);
	for(int ii = 0; ii < idim; ++ii) {
		for(int jj = 0; jj < jdim; ++jj) {
			for(int kk = 0; kk < kdim; ++kk) {
				// Populate points
				point_t pt;
				pt.x = ii/idim;
				pt.y = jj/jdim;
				pt.z = kk/kdim;
				x[idx] = pt;
				++idx;
			}
		}
	}
	
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{
    std::cout<<"matvec"<<std::endl;
	
	// Unfinished. Please see report.
	
}

void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs,
              vec_t &res, real_t &res_norm)
{
    std::cout<<"residual"<<std::endl;
	
	// Unfinished. Please see report.
	
}


