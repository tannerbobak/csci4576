#include "aux.h"

double a_(double x, double y, double z){ 

	return 12;
}

double v_(double x, double y, double z){

	double val = sin(4*PI*x)*sin(10*PI*y)*sin(14*PI*z);
	return val;
}

double f_(double x, double y, double z){

	double val = sin(4*PI*x)*sin(10*PI*y)*sin(14*PI*z)*(12 + 312*PI*PI);
	return val;
}

void copy(const vec_t &source, vec_t &target){

	int n_entries = source.size();
	target.resize(n_entries);
	for(int i(0); i < n_entries; i++){
		target[i] = source[i];
	}
}

void fill(const grid_t &x, vec_t &vec, std::function<double(double, 
		  double, double)> func){

	int n_entries = x.size();
	vec.resize(n_entries);
	for(int i(0); i < n_entries; i++){
		double x_ = x[i].x;
		double y_ = x[i].y;
		double z_ = x[i].z;
		vec[i] = func(x_, y_, z_);
	}
}
