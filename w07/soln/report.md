# CSCI-5576 Assignment 7
###### Shane A. Fiorenza
## Task 1

**1.** Submitted. Thoroughly commented so it should hopefully be intelligable.

**2.** Checking the correctness with known solution. 

Using a known solution, this is the semi-logarithmic plot we get for total residual norm as a function of discretization points:

![Residual scaling as a function of discretization](residual_scaling.png)


Since it converges to zero, we know our algorithm is indeed correct.

**3.** Strong scaling. 

First for the set p = {1,8,27}:

![Strong scaling no. 1](strong_scaling_a.png)

We see that wall-time decreases quite rapidly and that speedup is approximately linear with increasing threads.
This is quite good, and shows that we could still get an appreciable performance increase by adding more threads. 
Clearly, this is a highly parallel problem. 

Next, the set p = {1,4,9,16,25}:

![Strong scaling no. 2](strong_scaling_b.png)

This plot looks almost the exact same as the one above, just with better resolution.
This shows that our code does not rely on the number of threads being a perfect cube, which is desired. 

**4.** Weak Scaling. 

We use a constant grain size of n^3/p = 27mil and look at p = {8,27,64} for weak scaling:

![Weak scaling](weak_scaling.png)

The total wall-time hardly changes as a function of processors. 
Even with eight times as many processors, the code only takes ~10% longer to run.
This tells us two things: we have a highly parallel problem (which we already knew from our strong scaling), and that our communication costs are relatively low. 
