#!/bin/bash

#SBATCH --time=0:55:00     # walltime, abbreviated by -t
#SBATCH --nodes=2          # number of cluster nodes, abbreviated by -N
#SBATCH -o strong.out     # name of the stdout redirection file, using the job number (%j)
#SBATCH -e strong.err     # name of the stderr redirection file
#SBATCH --ntasks=27         # number of parallel process
#SBATCH --qos debug        # quality of service/queue (See QOS section on CU RC guide)

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

n=240;

echo Begin set p = {1, 8, 27} for strong scaling
for i in {1,8,27}; 
do 
	mpirun -n $i ./test_poisson.exe $n
done
echo Begin set p = {1, 4, 9, 16, 25} for strong scaling
for i in {1,4,9,16,25}; 
do 
	mpirun -n $i ./test_poisson.exe $n
done
