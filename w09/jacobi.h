#include "poisson.h"

/*
 * @comm MPI communicator (to compute residual)
 * @mv the matvec
 * @diag diagonal component of the matvec
 * @rhs the right hand side
 * @u0 initial guess
 * @eps_r relative tolerance
 * @eps_a absolute tolerance
 * @m check residual every m iterations
 * @k_max the maximum number of iteration
 * @u_final return value
 * @k the number of iteration taken
 */
void jacobi_solver(const MPI_Comm &comm, matvec_t &mv, 
                 vec_t &diag, vec_t &rhs, vec_t &u0, real_t eps_r, real_t eps_a, 
                 int k_max, vec_t &u_final, int &k);
