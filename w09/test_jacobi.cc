/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief test driver for Possion
 */

#include "aux.h"
#include <iostream>

/*
 * Modified version of Shane Fiorenza's Assignment 07 Solution used to drive the Jacobi solver.
 */

int main(int argc, char** argv){

	MPI_Init(&argc, &argv);
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// make sure input is correct
	if(argc != 2){
		if(world_rank == 0){
			printf("\nError: format is './test_jacobi.exe n', where");
			printf(" n is the number of\ndiscrete coordinates used to");
			printf(" describe each dimension after discretization.\n\n");
		}
		exit(1);
	}
	if(world_rank == 0){
		printf("%i threads with n = %i discretization points\n", 
				world_size, atoi(argv[1]));
	}

	// starting wall time
	real_t tic = MPI_Wtime();

    MPI_Comm grid_comm;
    int n = atoi(argv[1]); 
    grid_t x;
	// set up cartesian grid communicator
    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

    /** fill a and f based on x **/
    vec_t a, f;
	fill(x, a, n, a_);
	fill(x, f, n, f_); 

    // make a matvec object to pass to the residual function (residual
    // doesn't care how you do the matvec, it just passes an input and
    // expect and output.
    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, 
			std::ref(a), std::placeholders::_1, std::placeholders::_2);

    // now you can call mv(v,lv)

    // setup the problem for iteration
    vec_t v, rhs, res, diag, u_final;
    real_t res_norm;
    int k;

	// fill v based on x
	fill(x, v, n, v_);

	// fill diagonal array
	fill(x, diag, n, diag_);

	// copy f vec into rhs as our reference for the residual
	copy(f, rhs);

	size_t sz = x.size();

	u_final.reserve(sz);

	// attempt a solution
	jacobi_solver(MPI_COMM_WORLD, mv, diag, rhs, v, 1e-3, 1e-5, 10000, u_final, k);

	if(world_rank == 0){
		printf("Total iterations: %d\n", k);
		// ending wall time
		real_t toc = MPI_Wtime();
		printf("Total time elapsed: %g seconds\n", toc-tic);
	}

    /** cleanup **/
	MPI_Finalize();
    return 0;
}
