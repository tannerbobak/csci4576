#!/bin/bash

#SBATCH --time=0:55:00      # walltime, abbreviated by -t
#SBATCH --nodes=1           # number of cluster nodes, abbreviated by -N
#SBATCH -o residual.out     # name of the stdout redirection file, using the job number (%j)
#SBATCH -e residual.err     # name of the stderr redirection file
#SBATCH --ntasks=64         # number of parallel process
#SBATCH --qos debug         # quality of service/queue (See QOS section on CU RC guide)

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

echo "Begin Jacobi iteration."
for i in {256,512,758,1024}; 
do 
	mpirun -n 64 ./test_poisson.exe $i
done
