#include "aux.h"

/*
 * Modified from Shane Fiorenza's Assignment 07 Solution
 */

double a_(double x, double y, double z, double h){ 

	return 12;
}

double v_(double x, double y, double z, double h){

	double val = sin(4*PI*x)*sin(10*PI*y)*sin(14*PI*z);
	return val;
}

/* For assignment 7
double f_(double x, double y, double z){

	double val = sin(4*PI*x)*sin(10*PI*y)*sin(14*PI*z)*(12 + 312*PI*PI);
	return val;
}*/

double f_(double x, double y, double z, double h){

	double val = sin(2*PI*x)*sin(12*PI*y)*sin(8*PI*z);
	return val;
}

double diag_(double x, double y, double z, double h) {
	return a_(x,y,z,h) - 6.0/(h*h);
}

void copy(const vec_t &source, vec_t &target){

	int n_entries = source.size();
	target.resize(n_entries);
	for(int i(0); i < n_entries; i++){
		target[i] = source[i];
	}
}

void fill(const grid_t &x, vec_t &vec, int n, std::function<double(double, 
		  double, double, double)> func){

	int n_entries = x.size();
	double h = (double) 1 / (n - 1);
	vec.resize(n_entries);
	for(int i(0); i < n_entries; i++){
		double x_ = x[i].x;
		double y_ = x[i].y;
		double z_ = x[i].z;
		vec[i] = func(x_, y_, z_, h);
	}
}
