#include "jacobi.h"

/*
 * Tanner Bobak's implementation of the Jacobi solver, utilizing methods from Shane F.'s poisson residual functions.
 */

void jacobi_solver(const MPI_Comm &comm, matvec_t &mv,
                   vec_t &diag, vec_t &rhs, vec_t &u0, real_t eps_r, real_t eps_a,
                   int k_max, vec_t &u_final, int &k) {

	// MPI setup
	int world_size;
	MPI_Comm_size(comm, &world_size);
	int world_rank;
	MPI_Comm_rank(comm, &world_rank);

	// Variable setup
	vec_t res, u;
    int n = std::cbrt(world_size);
    real_t res_norm_0, res_norm;
    size_t n_entries = u0.size();
    u.reserve(n_entries);

    /*for(int i = 0; i < u0.size(); ++i) {
        printf("%f ", u0[i]);
    }
    printf("\n");*/

    // Compute the initial solution residual
    residual(comm, mv, u0, rhs, res, res_norm_0);

    //printf("%f\n",res_norm_0);

	// sum local residuals to get global residual
	real_t tot_res_norm = 0;
	real_t local_res_norm[world_size];
	// use MPI_Gather to get local value from all processors
	MPI_Gather(&res_norm_0, 1, MPI_DOUBLE, local_res_norm, 1, 
			   MPI_DOUBLE, 0, MPI_COMM_WORLD);
	if(world_rank == 0){
		// normalize residual w.r.t. total number of points, n^3 
		int N = n*n*n;
		for(int i(0); i < world_size; i++){
			tot_res_norm += local_res_norm[i]/N;
		}
	}

	// Broadcast the total residual norm to all processes.
	MPI_Bcast(&tot_res_norm, 1, MPI_DOUBLE, 0, comm);

    res_norm_0 = tot_res_norm;
    u = u0;
    k = 1;

    while(k <= k_max) { // Iteratively solve until max iterations are reached

    	// Compute the new residual
        residual(comm, mv, u, rhs, res, res_norm);

    	// sum local residuals to get global residual
		tot_res_norm = 0;
		// use MPI_Gather to get local value from all processors
		MPI_Gather(&res_norm, 1, MPI_DOUBLE, local_res_norm, 1, 
				   MPI_DOUBLE, 0, MPI_COMM_WORLD);
		if(world_rank == 0){
			// normalize residual w.r.t. total number of points, n^3 
			int N = n*n*n;
			for(int i(0); i < world_size; i++){
				tot_res_norm += local_res_norm[i]/N;
			}
		}

		// Once again broadcast the residual to make sure everyone is on the same page.
		MPI_Bcast(&tot_res_norm, 1, MPI_DOUBLE, 0, comm);

		// Shows the slow convergence
	    if(world_rank == 0) printf("Iteration %d, residual norm: %f\n",k,res_norm);

		res_norm = tot_res_norm;

		// If convergence criteria are met, break.
    	if(res_norm < eps_r*res_norm_0 + eps_a) {
    		u_final = u;
    		break;
    	}

    	// Update the candidate solution.
    	for(size_t i = 0; i < n_entries; ++i) {
    		u[i] = (1/diag[i]) *  (res[i] - diag[i]*u[i]);
    	}

    	++k;
    }
}
