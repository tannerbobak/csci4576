# Assignment 09
Tanner Bobak

CSCI 4576

November 16, 2018

---

## Task 1

There were some substantial difficulties encountered in this assignment, however in the end I believe I have a rudimentary implementation! In my implementation I stuggled to compute the residuals and get the solution to converge properly, partly because of my relative inexperience and confusion over the linear algebra at work and partly because I did not have my own implementation of Assignment 07. Therefore I had to port one of the assignment solutions into this work. As a result, my residual norms are significant and are probably not correct. As a result, I increased the value of `eps_r` and `eps_a` in my implementation so that some convergence could still be observed. I also set `k_max` to be 10,000. The data are plotted below. It can be seen that as the number of discretization points increases, the number of iterations required for convergence also increases. In fact, the number of iterations increases so much for my imlementation that the problems with 758 and 1,024 discretization points did not finish within the allowed 1hr wall clock time. Given a longer maximum wall time or more time to sort out the issues with my port of the resisdual calculation I am sure I could bring down the time required to process this job.

Points (n) | Iterations (k)
--- | ---
256 | 879
512 | 3,259
758 | DNF (>1,303)
1,024 | DNF
