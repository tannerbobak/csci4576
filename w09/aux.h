#include "jacobi.h" 

/*
 * Modified from Shane Fiorenza's Assignment 07 Solution
 */

constexpr double PI = 3.14159265358979323846;

double a_(double x, double y, double z, double h);
double v_(double x, double y, double z, double h);
double f_(double x, double y, double z, double h);
double diag_(double x, double y, double z, double h);

void copy(const vec_t &source, vec_t &target);
void fill(const grid_t &x, vec_t &vec, int n, std::function<double
		 (double, double, double, double)> func);
