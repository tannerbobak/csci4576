#!/bin/bash

#SBATCH --job-name mpi_test
#SBATCH --qos debug
#SBATCH --nodes 2
#SBATCH --ntasks-per-node 4
#SBATCH --time 00:10:00
#SBATCH --output mpi_test.out
#SBATCH -e mpi_test.err

module load slurm

module load mpich/mpich-3.1.2_intel-13.0.0

# PMI-2 must be specified explicitly when using MPICH
srun --mpi=pmi2 helloworld