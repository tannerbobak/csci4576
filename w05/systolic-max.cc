#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <mpi.h>
#include "slurp_file.h"

int main(int argc, char** argv){

    if (argc<2){
        std::cerr<<"File name is a required argument"<<std::endl;
        return 1;
    }

    MPI_Init(&argc,&argv);
    int rank,nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::vector<data_t> data;
    slurp_file_line(argv[1] /* file name */, rank /* line number */, data);

    // Sequentially find max.
    data_t mx(data[0]);
    for (int i(0);i<data.size();++i)
        if (data[i]>mx) mx=data[i];

    std::cout<<"[rank "<<rank<<"] "<<mx<<std::endl;

    MPI_Status stat;
    if(rank == nproc-1) {
        MPI_Send(&mx, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD);
    } else if(rank == 0) {
        int rmx;
        MPI_Recv(&rmx, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, &stat);
        if(rmx > mx)
            mx = rmx;
        std::cout << "Global Max: " << mx << std::endl;
    } else {
        int rmx;
        MPI_Recv(&rmx, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD, &stat);
        if(rmx > mx)
            mx = rmx;
        MPI_Send(&mx, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}
