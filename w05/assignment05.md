# Assignment 05

Tanner Bobak

CSCI 4576

October 9, 2018

---

## Task 1

### Question 1

The `mpicc` wrapper passes on the following options to the gcc compiler, as given by the command `mpicc -show`:

```
-I/curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/include -L/curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib/release_mt -L/curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib -Xlinker --enable-new-dtags -Xlinker -rpath -Xlinker /curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib/release_mt -Xlinker -rpath -Xlinker /curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib -Xlinker -rpath -Xlinker /opt/intel/mpi-rt/5.1/intel64/lib/release_mt -Xlinker -rpath -Xlinker /opt/intel/mpi-rt/5.1/intel64/lib -lmpifort -lmpi -lmpigi -ldl -lrt -lpthread
```

### Question 2

I have compiled my mpi_hello_world.exe program using the `mpicc` compiler wrapper after running the commands `source config.octane.rc` and `export MPICXX=mpicc`.

## Task 2

### Question 3, 4

`local-max.cc` was copied from the class repository's `mpi.cc` file. It was then modified to compute the global max by first having each thread computing a local max. Then, the highest ranked thread passed on its max to the next lower ranking thread, who would compare the recieved max to the local maximum and pass on the greater of the two. This continues until thread 0 is reached, at which point the global max can be displayed.

### Question 5

To implement systolic matrix vector multiplication, `Ax=b`, on MPI, each thread first grabs the last line from the matrix file, the `x` vector. Then, each thread grabs a column of the array from file, multiplying the column's elements by the corresponding element in `x`. The resulting vector is stored and the thread grabs another column `nproc` further down in the file. This way, each thread is grabbing and computing separate columns. The same multiplication process is carried out on the new column and added to the previous result.

Once one node's workshare is complete, it either sends its own result to the next lowest ranked processor (if it is the highest ranked processor) or waits for a higher ranked processor's result, adds that to its own, and then passes the new result along. This continues until thread 0, where the final vector is computed and printed.

My implementation requires 2 arguments: the file to read from and the dimension of the matrix `n`. Therefore, the usage is `mpirun -n <nproc> ./systolic-matvec.exe <filename> <n>` after building with `make`.

### Question 6

The paper linked in the assignment details the implications of Amdahl's Law as processors continue to have more and more computational cores. Notably, it examines different parallel processing architectures of base-core equivalents (BCEs) -- symmetric, asymmetric, and dynamic -- and the theoretical speedup and given by Amdahl's law with these different implementations. The results compare the benefits of moving to increasingly parallel software, but remind the reader that it is still important to continue developing sequential performance on each core. The paper also presents how some of the trade-offs between more cores on the available chip area and speed can be counteracted using asymmetric chip designs (where one core is more powerful than the others) or dynamic architectures (where cores work together in a sequential mode but can switch to a parallel mode when a task demands it). Even though this paper is nearly a decade old now, it is still relevant as even consumer chips now routinely have in excess of 8 processing cores, and workloads move towards parallelization.