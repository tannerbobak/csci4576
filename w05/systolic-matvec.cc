#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <mpi.h>
#include <cstdlib>
#include <unistd.h>
#include "slurp_file.h"

int main(int argc, char** argv){

    if (argc<3){
        std::cerr<<"Usage: ./systolic-matvec.exe <file to read> <size of matrix>"<<std::endl;
        return 1;
    }

    // Initialize
    MPI_Init(&argc,&argv);
    int rank,nproc,n;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    n = atoi(argv[2]);

    // Get x
    std::vector<data_t> x; 
    slurp_file_line(argv[1], n, x);

    // Result to pass on.
    std::vector<data_t> result(n,0);

    // Work per processor
    int div = n/nproc;
    for(int itr = rank; itr < n; itr += nproc) {
        std::vector<data_t> line;
        slurp_file_line(argv[1], itr, line);

        // Update result
        for(int item = 0; item < n; ++item)
            result[item] += x[item]*line[item];
    }

    // At this point, result contains what each processor's workshare is.

    // sleep(rank); // Sleep for seconds proportional to rank,

    // printf("Thread %i: result:", rank);
    // for(int i = 0; i < n; ++i) printf("%i ", result[i]);
    // printf("\n");

    MPI_Status status;
    if(rank == 0 && nproc > 1) {
        // Leftmost
        std::vector<data_t> recieved;
        recieved.resize(n);
        MPI_Recv(&recieved[0], n, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);

        // printf("Thread %i recieved: ", rank);
        // for(int i = 0; i < n; ++i) printf("%i ", recieved[i]);
        // printf("\n");

        for(int item = 0; item < n; ++item) result[item] += recieved[item];

        // printf("Thread %i final result: ", rank);
        // for(int i = 0; i < n; ++i) printf("%i ", result[i]);
        // printf("\n");
       
        printf("Result: ");
        for(int i = 0; i < n; ++i) printf("%i ", result[i]);
        printf("\n");
    } else if(rank == nproc - 1) {
        // Rightmost
        MPI_Send(&result[0], n, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
    } else {
        // Middle
        std::vector<data_t> recieved;
        recieved.resize(n);
        MPI_Recv(&recieved[0], n, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, &status);

        // printf("Thread %i recieved: ", rank);
        // for(int i = 0; i < n; ++i) printf("%i ", recieved[i]);
        // printf("\n");

        for(int item = 0; item < n; ++item) result[item] += recieved[item];

        // printf("Thread %i passing on: ", rank);
        // for(int i = 0; i < n; ++i) printf("%i ", result[i]);
        // printf("\n");

        MPI_Send(&result[0], n, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}
