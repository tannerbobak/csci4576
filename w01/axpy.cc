#include "axpy.h"

void axpy(size_t n, double a, const double *x, double *y) throw (std::runtime_error) {
	for (int i = 0; i < n; i++) {
		y[i] += a * x[i];
	}
}