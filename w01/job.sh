#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH --nodes=1
#SBATCH -o axpy-%j.out
#SBATCH -e axpy-%j.err
#SBATCH --ntasks 1
#SBATCH --qos debug

echo "Running on $(hostname --fqdn)"
module load intel

./test_axpy.exe
