#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH --nodes=1
#SBATCH -o axpy-%j.out
#SBATCH -e axpy-%j.err
#SBATCH --ntasks 16
#SBATCH --qos debug

echo "Running on $(hostname --fqdn)"
module load intel

echo "Number of threads = 1"
OMP_NUM_THREADS=1 ./test_axpy.exe

echo "Number of threads = 2"
OMP_NUM_THREADS=2 ./test_axpy.exe

echo "Number of threads = 4"
OMP_NUM_THREADS=4 ./test_axpy.exe

echo "Number of threads = 8"
OMP_NUM_THREADS=8 ./test_axpy.exe

echo "Number of threads = 16"
OMP_NUM_THREADS=16 ./test_axpy.exe
