# Assignment 01
Tanner Bobak

CSCI 4576

9/5/18

--- 

## Task 1

### Question 1
I ran `man bash` and searched for `PATH` and found the following:

```
PATH   The  search  path  for  commands.  It is a colon-separated list of directories in which the shell looks for commands (see COMMAND EXECUTION below).  A zero-length (null) directory name in the
       value of PATH indicates the current directory.  A null directory name may appear as two adjacent colons, or as an initial or trailing colon.  The default path is system-dependent, and is  set
       by the administrator who installs bash.  A common value is "/usr/gnu/bin:/usr/local/bin:/usr/ucb:/bin:/usr/bin".
```
	   
### Question 2
Running `man ld` produces the linker manual page. It contains the paragraph:

```
The linker uses the following search paths to locate required shared libraries:

           1.  Any directories specified by -rpath-link options.

           2.  Any directories specified by -rpath options.  The difference between -rpath and -rpath-link is that directories specified by -rpath options are included in the executable and used at
               runtime, whereas the -rpath-link option is only effective at link time. Searching -rpath in this way is only supported by native linkers and cross linkers which have been configured with the
               --with-sysroot option.

           3.  On an ELF system, for native linkers, if the -rpath and -rpath-link options were not used, search the contents of the environment variable "LD_RUN_PATH".

           4.  On SunOS, if the -rpath option was not used, search any directories specified using -L options.

           5.  For a native linker, search the contents of the environment variable "LD_LIBRARY_PATH".

           6.  For a native ELF linker, the directories in "DT_RUNPATH" or "DT_RPATH" of a shared library are searched for shared libraries needed by it. The "DT_RPATH" entries are ignored if "DT_RUNPATH"
               entries exist.

           7.  The default directories, normally /lib and /usr/lib.

           8.  For a native linker on an ELF system, if the file /etc/ld.so.conf exists, the list of directories found in that file.

           If the required shared library is not found, the linker will issue a warning and continue with the link.
```

### Question 3
I ran `echo ${HOME}` to find the path to my home directory. The result is:

```
/home/tabo5518
```

## Task 2

### Question 4
Done!

### Question 5
I ran `module show intel` to see the details about Summit's Intel modules. The Intel module contains Fortran, C, and C++ compilers. It adds the directory `/curc/sw/gcc/5.4.0/bin` to `PATH` and `/curc/sw/gcc/5.4.0/lib64` to `LD_LIBRARY_PATH`.

### Question 6
After loading the Intel module, the MPI implementation `impi/17.3` was displayed when running `module avail`. This is a consequence of the layered hierarchical structure of Lmod. MPI implementations depend on compilers. Running `module spider impi/17.3` confirms this, listing that it is dependent on `intel/17.4`.

### Question 7
The motivation for a hierarchical module system is to ensure that all dependencies for a piece of software are met when it is loaded. In addition, the hierarchical model allows multiple versions of software (e.g. CUDA 8.0 and CUDA 9.0) to be installed on the computing cluster, permitting many users to work concurrently. Finally, the module system allows for a dynamic way to change environment variables that control how the system behaves.

## Task 3

### Question 8
The Intel module sets five environment variables: `CC=icc`, `FC=ifort`, `CXX=icpc`, `AR=xiar`, and finally `LD=xild`.

## Task 4

### Question 9
On my machine (AMD cpu) running Ubuntu 18.04.1 LTS, running `gcc -v` prints the following:

```
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/7/lto-wrapper
OFFLOAD_TARGET_NAMES=nvptx-none
OFFLOAD_TARGET_DEFAULT=1
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Ubuntu 7.3.0-16ubuntu3' --with-bugurl=file:///usr/share/doc/gcc-7/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++ --prefix=/usr --with-gcc-major-version-only --with-as=/usr/bin/x86_64-linux-gnu-as --with-ld=/usr/bin/x86_64-linux-gnu-ld --program-suffix=-7 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-libmpx --enable-plugin --enable-default-pie --with-system-zlib --with-target-system-zlib --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 7.3.0 (Ubuntu 7.3.0-16ubuntu3)
```

This indicates that I have the GNU C compiler installed. I also have the GNU C++ compiler installed, and running `g++ -v` yields

```
Using built-in specs.
COLLECT_GCC=g++
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/7/lto-wrapper
OFFLOAD_TARGET_NAMES=nvptx-none
OFFLOAD_TARGET_DEFAULT=1
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Ubuntu 7.3.0-16ubuntu3' --with-bugurl=file:///usr/share/doc/gcc-7/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++ --prefix=/usr --with-gcc-major-version-only --with-as=/usr/bin/x86_64-linux-gnu-as --with-ld=/usr/bin/x86_64-linux-gnu-ld --program-suffix=-7 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-libmpx --enable-plugin --enable-default-pie --with-system-zlib --with-target-system-zlib --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 7.3.0 (Ubuntu 7.3.0-16ubuntu3)
```

### Question 10
The code for `axpy` can be viewed in [axpy.cc](https://bitbucket.org/tannerbobak/csci4576/src/master/w01/axpy.cc). This file, along with the header `axpy.h` and test file `test_axpy.cc` are compiled with the following commands:

```
# Compile
g++ -c axpy.cc
g++ -c test_axpy.cc

# Link
g++ -o test_axpy axpy.o test_axpy.o
```

Alternatively, the `make` command can be run with the makefile [here](https://bitbucket.org/tannerbobak/csci4576/src/master/w01/Makefile). The makefile contains commands for both GNU and Intel compilation. 

## Task 5

### Question 11
I created the [job.sh](https://bitbucket.org/tannerbobak/csci4576/src/master/w01/job.sh) file and submitted it to Summit using `sbatch job.sh`, spawning job number 1177438. I made sure that the `job.sh` file ran a version of the code compiled with the intel compiler, that is I used `make intel` with the makefile I created. To monitor the job's progress, however short, I used the command `squeue -u tabo5518 -l` which prints a long-format output of the status of jobs created by me. I aliased this in my `.bashrc` as the command `q`. 

Two files were generated as output. They can be viewed in the repository. The `.out` file contains

```
Running on shas0516.rc.int.colorado.edu
Calling axpy - Passed
```

and the `.err` file is empty. This matches up with the output I got on my machine.

## Task 6

### Question 12
For this question I first copied the original `axpy.cc` file to `axpy_openmp.cc`. In this file I added the OpenMP directives listed in the assignment documentation. I then modified my makefile to have another configuration that builds for OpenMP using the `-qopenmp` flag to eliminate the warning. Now I use the command `make omp` to build `test_axpy.exe`. 

The behavior of the program when run with OpenMP is unchanged except now the time taken to run is displayed. By default, it appears that the execution speed is unchanged from the compilation without OpenMP.

### Question 13
Running the OpenMP compiled version of axpy produced some interesting results! To tests the speeds, I spawned two jobs: first job 1177485 with `--ntasks` set to 1, then job 1177488 with `--ntasks` set to 16. 

In the first run, the execution time remained approximately the same for all numbers of threads - about 0.25 seconds. However, there was a slight upward trend in the execution time with increasing number of threads, and the 16-thread run took nearly twice as long as the one thread run! Maybe this is a result of context switching issues because only one core was allocated?

In the second run, there was a far more noticable decrease in the execution time with increasing numbers of threads, starting around 0.25 seconds for one thread and decreasing to only 0.05 seconds for 16 threads! This may be the result of the load balancing over multiple cores more effectively. 

## Task 7

### Question 14 through 17
I have read the user guides and linked documents. I also read through `test_axpy.cc` and I think I have a rough idea of its function.