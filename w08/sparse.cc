#include "sparse.h"

sparse::sparse(size_t m, size_t n, storage_type t) :
        mm(m), nn(n), type(t) {}

void sparse::transpose(sparse &B) {
    /*
     * Not sure if this will be finished by the time you pull from BitBucket. I've
     * outlined my plan below to finish the transpose function but I'm having a really
     * hard time with C/C++ and MPI. :(
     *
     * The utility functions for changing between CSR/COO appear to work great! I planned
     * to use them here but couldn't wrap my head around the MPI stuff.
     */ 
    int rank, nproc;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    size_t rows_per_p = mm / nproc; // assume mm is divisible by nproc

    size_t splits[p];
    size_t offsets[p];

    for(size_t i = 0; i < p; ++i) {
        splits[p] = row[(i+1)*rows_per_p] - row[i*rows_per_p];
        if(i == 0) offsets[i] = 0;
        else offsets[i] = splits[i - 1] + splits[i]; 
    }
    
    size_t data_buf[splits[rank]];
    size_t col_buf[splits[rank]];
    size_t row_buf[rows_per_p+1];
    size_t data_to_send = &data[0];
    size_t cols_to_send = &column[0];
    size_t rows_to_send = &row[0];
    MPI_Scatterv(data_to_send,splits,offsets,MPI_DOUBLE,data_buf,splits[rank],
        MPI_DOUBLE,0,MPI_COMM_WORLD);
    MPI_Scatterv(cols_to_send,splits,offsets,MPI_DOUBLE,col_buf,splits[rank],
        MPI_DOUBLE,0,MPI_COMM_WORLD);
    MPI_Scatter(rows_to_send,rows_per_p,MPI_DOUBLE,row_buf,MPI_DOUBLE,0,MPI_COMM_WORLD);

    // Change the values in the rows to accurately reflect sub-matrix CSR format. i.e., subtract
    // the value of the first element from all the subsequent elements to obtain the new row 
    // infomation

    // Convert to COO by first putting the new array in a sparse object and calling
    // "change_format"

    // Perform the transpose here by swapping the row and column

    // Call gatherv and gather to collect all information 

    // Fix the rows by putting everything back into one big array and calling "change format" to
    // CSR

    // Done

}

// Swaps storage format from COO to CSR and vice-versa.
void sparse::change_format(storage_type t) {
    if(t == type) return;

    if (t == storage_type::CSR) // Convert to CSR from COO
    {
        std::vector<size_t> sorting_index;
        sort_index(row, sorting_index);
        
        std::vector<data_t> data2;
        data2.resize(data.size());
        std::vector<size_t> column2;
        column2.resize(column.size());
        std::vector<size_t> row2;
        row2.resize(row.size());

        for(size_t i = 0; i < sorting_index.size(); ++i) {
            size_t move_from = sorting_index[i];
            data2[i] = data[move_from];
            column2[i] = column[move_from];
            row2[i] = row[move_from];
        }

        data = data2;
        column = column2;
        row = row2;

        std::vector<size_t> row3;
        row3.reserve(mm+1);
        row3[0] = 0;
        for(size_t i = 1; i < mm+1; i++) {
            row3[i] = row3[i-1] + std::count(row.begin(), row.end(), i-1);
        }
        row = row3;
    }
    else // Convert to COO from CSR
    {
        std::vector<size_t> row2;
        row2.reserve(nnz);
        size_t current_row = 0;
        for(size_t i = 0; i < mm; ++i) {
            for(size_t j = row[i]; j < row[i+1]; ++j) {
                row2[j] = i;
            }
        }
        row = row2;
    }

    type = t;
}

// Generates an array "idx" that who's values point to the indexes in "v" that
// will be in ascending order. 
void sparse::sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx) {
    size_t n(0);
    idx.resize(v.size());
    std::generate(idx.begin(), idx.end(), [&]{ return n++; });
    std::sort(idx.begin(), idx.end(), 
              [&](size_t ii, size_t jj) { return v[ii] < v[jj]; } );
}

// Generates a random sparse matrix.
void sparse::generate_matrix(size_t m, size_t n) {
    srand(time(nullptr));
    // Loop through elements
    for(size_t i = 0; i < m; i++) {
        for(size_t j = 0; j < n; j++) {
            if((rand() % 4) == 0) { // 25% of filling with a nonzero
                data_t v = (data_t) ((rand() % 9) + 1);
                data.push_back(v); // values between 1 and 9 (inclusive)
                row.push_back(i);
                column.push_back(j);
            }
        }
    }
    mm = m;
    nn = n;
    type = storage_type::COO;
}

// Converts a COO format matrix to a dense matrix representation.
data_t** sparse::get_dense_matrix() {
    data_t** A = new data_t*[mm];
    for(size_t i = 0; i < mm; ++i) {
        A[i] = new data_t[nn];
        for(size_t j = 0; j < nn; ++j) {
            A[i][j] = 0;
        }
    }

    for(size_t idx = 0; idx < data.size(); ++idx) {
        size_t r = row[idx];
        size_t c = column[idx];
        A[r][c] = data[idx];
    }
    
    return A;
}

// Prints the matrix, assuming it is in COO format.
void sparse::print_matrix() {
    data_t** A;
    A = get_dense_matrix();
    printf("Matrix = \n");  
    for(size_t i = 0; i < mm; ++i) {
        for(size_t j = 0; j < nn; ++j) {
            printf("%0.0f ", A[i][j]);
        }
        printf("\n");
    }

    delete[] A;
}
