#include <vector>
#include <algorithm>
#include <mkl.h>
#include <cstdlib>
#include <cstdio>
#include <time.h>
#include <mpi.h>

typedef double data_t;

class sparse {
  public:
    enum storage_type {CSR, COO};
    std::vector<data_t> data;
    std::vector<size_t> column;
    std::vector<size_t> row;
    size_t mm,nn; /* (local) matrix dimensions */
    size_t nnz;
    storage_type type;
   
    sparse(size_t m, size_t n, storage_type t = CSR);
    void transpose(sparse &B); /* returns transpose in B */
    void generate_matrix(size_t m, size_t n);
    data_t** get_dense_matrix();
    void print_matrix();
    void change_format(storage_type t);
  private:
    void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx);
};
