#include "sparse.h"
#include <cstdio>

int main(int argc, char** argv) {
	
	// Usage: mpirun -n <ncores> ./test_sparse.exe <side length of matrix>

	if(argc < 3) {
		printf("Side lengths of matrix are required arguments.\n");
		printf("Usage: mpirun -n <ncores> ./test_sparse.exe <mrows> <ncols>\n");
		exit(1);
	}

	size_t m(std::atoi(argv[1]));
	size_t n(std::atoi(argv[2]));

    sparse s(m,n,sparse::storage_type::CSR);
    s.generate_matrix(s.mm,s.nn);
    /*s.print_matrix();

    printf("Converting to CSR...\n");
    s.change_format(sparse::storage_type::CSR);
    s.print_matrix();*/

    MPI_Init(&argc, &argv);
    sparse b(m,n,sparse::storage_type::CSR);
    s.change_format(sparse::storage_type::CSR);
    s.transpose(b);
    MPI_Finalize();

    return 0;
}
