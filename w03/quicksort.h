/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 1 $
 * @tags $Tags: tip $
 * @date $Date: Tue Sep 26 18:22:46 2017 -0600 $
 *
 * @brief quicksort header
 */

#include <vector>

typedef std::vector<long> vec_t;

void quicksort(const vec_t &x, vec_t &y);
