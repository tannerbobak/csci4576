#!/bin/bash
CORES=1
while [ $CORES -lt 9 ]; do
	echo "Cores: ${CORES}"
	export OMP_NUM_THREADS=${CORES}
	./test_quicksort.exe $(($1 * $CORES))
	let CORES=CORES+1
done