#include <cstdlib>
#include <cstdio>
#include "omp.h"

long fib(long n) {
	printf("Thread %d in fib(long n) where n=%ld.\n", omp_get_thread_num(), n);
    long i(0), j(0);
    if (n<2) return 1;

    #pragma omp task shared(i)
    {
    	printf("Thread %d in fib(long %ld) calculating i.\n", omp_get_thread_num(), n);
    	i = fib(n-1);
    	printf("Thread %d in fib(long %ld) i=%ld.\n", omp_get_thread_num(), n, i);
    }

    #pragma omp task shared(j)
    {
    	printf("Thread %d in fib(long %ld) calculating j.\n", omp_get_thread_num(), n);
    	j = fib(n-2);
    	printf("Thread %d in fib(long %ld) j=%ld.\n", omp_get_thread_num(), n, j);
    }
    
    // With taskwait
    //
    // ---
    //
    #pragma omp taskwait
    {
    	printf("Thread %d in fib(long %ld) calculating i+j where i=%ld and j=%ld.\n", omp_get_thread_num(), n, i, j);
    	return i + j;
    }

    // Without taskwait
    //
    // ---
    //
    // printf("Thread %d in fib(long %ld) calculating i+j where i=%ld and j=%ld.\n", omp_get_thread_num(), n, i, j);
    // return i + j;
}

int main(int argc, char** argv) {

	int nt = omp_get_max_threads();
	printf("Starting Fibonacci with %d threads...\n", nt);

    long n, v;

    n = (long) atoi(argv[1]);

    #pragma omp parallel shared (n, v)
    {

    	int thr = omp_get_thread_num();
    	printf("What up it's ya boi thread %d comin' right at ya.\n", thr);

        #pragma omp single
        {
        	printf("Thread %d staring Fibonacci sequence.\n", omp_get_thread_num());
        	v = fib(n);
        	printf("Thread %d Fibonacci sequence complete. v=%ld\n", omp_get_thread_num(), v);
        }
    }

  return 0;
}