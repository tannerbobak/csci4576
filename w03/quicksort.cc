#include "quicksort.h"
#include <cstddef>
#include <vector>
#include <cstdio>
#include "omp-utils.h"

typedef std::vector<long> vec_t;

#ifdef _OPENMP 

// Parallel quicksort

// Swaps two elements in the array.
void swap(vec_t &x, int i, int j) {
	long tmp = x[i];
	x[i] = x[j];
	x[j] = tmp;
}

// Places the pivot element (the element that started at end_idx)
// and ensures that all elements to the left of the pivot are lesser
// in value and all elements to the right of the pivot are greater.
int partition(vec_t &x, int start_idx, int end_idx) {
	// Last element is the pivot.
	int pivot = x[end_idx];

	// Index below where to place pivot at. The pivot will be placed
	// at index i.
	int i = start_idx - 1;

	// Iterate through element and move elements that are smaller than
	// the pivot to the left
	for(int j = start_idx; j <= end_idx - 1; ++j) {
		if(x[j] <= pivot) {
			++i;
			swap(x, i, j);
		}
	}

	// Place pivot
	swap(x, i+1, end_idx);
	
	return i+1;
}

// Internal quicksort method that sorts vec_t y between start_idx and
// end_idx, both inclusive. The last parameter is true or false and
// indicates whether is is worth it to try dividing the problem.
void quicksort_idx(vec_t &y, int start_idx, int end_idx, int task_me) {	
	// Store the idx of the pivot.
    int pivot; 

    // Base case is that the array to sort is size 1. If it is, that is,
    // if start_idx >= end_idx, then it is sorted. Otherwise, continue dividing.
    if (start_idx < end_idx) {

    	// Place the pivot in the current array at the correct location. This
    	// splits the array into two parts that can be sorted in parallel.
        pivot = partition(y, start_idx, end_idx);

        // For problem sizes less than about the constant below, it is experimentally
        // determined that is not worth the parallel overhead to continue splitting the task.
        if(task_me)
        	task_me = (end_idx - start_idx - 1)/2 > 32768;

        if(task_me) {
        	// Sort the left half of the array as a separate task. Define y as shared 
	        // so that all tasks operate on the same array.
	        #pragma omp task shared(y)
	        quicksort_idx(y, start_idx, pivot - 1, true);

	        // Sort the right half of the array as a separate task. Define y as shared 
	        // so that all tasks operate on the same array.
	        #pragma omp task shared(y)
	        quicksort_idx(y, pivot + 1, end_idx, true);
        } else {
        	// Sort the left half of the array as a separate task. Define y as shared 
	        // so that all tasks operate on the same array.
	        quicksort_idx(y, start_idx, pivot - 1, false);

	        // Sort the right half of the array as a separate task. Define y as shared 
	        // so that all tasks operate on the same array.
	        quicksort_idx(y, pivot + 1, end_idx, false);
        }

        
    }
}

// Call this method from test_quicksort.cc.
void quicksort(const vec_t &x, vec_t &y) {

	// Copy x into y so a mutable data type can be sorted. This takes a long
	// time to do and would be much faster if the parameter array x was sorted
	// itself.
	y = x;

	// Start the parallel block. Quicksort on the whole array is started 
	// by a single thread. `quicksort_idx() quicksorts between two indices
	// in the given array.
	#pragma omp parallel
   	{
   		#pragma omp single
		{
			quicksort_idx(y, 0, y.size() - 1, true);
		}
	}
}

#else 
// Recursive serial quicksort implementation for testing, 
// adapted from https://www.geeksforgeeks.org/quick-sort/

// Puts pivot element in the right location in the array.
int partition(vec_t &x, int low, int high) {
	// Last element is the pivot.
	int piv = x[high];

	int i = low - 1; // Index below where to place pivot at.

	for(int j = low; j <= high - 1; ++j) {
		if(x[j] <= piv) {
			++i;

			// Move appropriate element left of where to put pivot.
			long xj = x[j];
			x[j] = x[i];
			x[i] = xj;
		}
	}

	// Place pivot
	long xhigh = x[high];
	x[high] = x[i + 1];
	x[i+1] = xhigh;
	return i+1;
}

void quicksort_idx(vec_t &x, int low, int high) {
	if(low < high) {
		int split = partition(x, low, high);

		quicksort_idx(x, low, split - 1);
		quicksort_idx(x, split + 1, high);
	}
}

void quicksort(const vec_t &x, vec_t &y) {
	y = x;
	quicksort_idx(y, 0, y.size() - 1);
}
#endif