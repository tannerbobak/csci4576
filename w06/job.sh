#!/bin/bash

#SBATCH --job-name hpsc-scale
#SBATCH --qos debug
#SBATCH --nodes 1
#SBATCH --cpus-per-task 24
#SBATCH --time 01:00:00
#SBATCH --output hpsc-scale.out
#SBATCH -e hpsc-scale.err

echo "Running on $(hostname --fqdn)";

source config.summit.rc

echo "Testing histogram scaling...";

for i in {1,2,4,8,12,16,20,24}; do
	export OMP_NUM_THREADS=$i;
	echo "Threads=${i}";
	for sz in {100,100000,1000000000}; do
		for bins in {10,1000,10000}; do
			echo "Strong scaling";
			echo "1e${sz} Elements, 1e${bins} Bins";
			echo "No outlier:";
			./test_histogram.exe $sz $bins;
			echo "Outlier:";
			./test_histogram.exe $sz $bins $bins;

			echo "Weak scaling";
			echo "1e${sz} Elements/Core, 1e${bins} Bins/Core";
			echo "No outlier:";
			./test_histogram.exe $((i*sz)) $((i*bins));
			echo "Outlier:";
			./test_histogram.exe $((i*sz)) $((i*bins)) $((i*bins));
		done
	done
done

echo "Testing quicksort scaling...";

for i in {1,2,4,8,12,16,20,24}; do
	export OMP_NUM_THREADS=$i;
	echo "Threads=${i}";
	for grain in {1000000,10000000}; do
		echo "Strong scaling with size ${grain}";
		./test_quicksort.exe $grain;
		echo "Weak scaling with size ${grain}/core";
		./test_quicksort.exe $((grain*i));
	done
done
