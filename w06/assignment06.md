# Assignment 06
Tanner Bobak

CSCI 4576

October 17, 2018

---

## Task 1

### Question 1

#### Histogram Summary 

Histogram operates in four major stages. First, the minimum and maximum of the data is found via a parallel reduction. The minimum and maximum values that are found are used in the next step: population of the bin boundaries. In this step, all elements of the bin boundaries list are iterated through in parallel. The values of the bin limits are assigned to each element. Next, the vector of vectors that stores the counts of elements in each bin is zeroed out in parallel so that each vector is all zeros. That way, in the following step where the data is counted, each count will have a definite value (as opposed to undefined if the memory was just reserved) and not experience false sharing (because each thread gets its own local vector). Each thread then iterates over a section of the data and populates bins accordingly. Finally, the totals in each thread's bins are added up in parallel to produce the final histogram. My implementation of histogram was very similar to this one, with the notable difference being that my code did not count up the bin totals in parallel. Instead, that part was carried out in a critical section because at the time I was not able to figure out a parallel implementation without race conditions.

#### Quicksort Summary

Quicksort works by finding a pivot value and placing arrays that are all less than or equal to the pivot element to the left and all elements greater than the pivot element to the right. The implementation is recursive, so each half is then quicksorted itself until a base case is reached where the array is length 1. The given implementation also has parameters that select task partitioning strategies based on the current array size. Large arrays (>NTSK) are partitioned serially to avoid parallel overhead. Then, when arrays are sufficiently small (<=NMIN), no more parallel tasks are spawned and the sorting is evaluated serially again. Partitioning of the array is also carried out in parallel unless the array is too small. This implementation is different then mine because my sorting strategy starts off in parallel but switches to serial when arrays are sufficiently small. There is no additional strategy change for partitioning. However, the recursive quicksort algorithm is approximately the same for my implementation.

### Question 2

The algorithms were run on Summit with strong and weak scaling. The results are plotted below with number of threads (1-24) on the x-axis and log10(wall time in seconds) on the y-axis. 

It can be seen that in histogram, outliers don't appreciably impact the speed of execution. Additionally, it is visible that parallelization is really only helpful for greater than 100 elements. With 100 elements, execution time actually increased with more cores!

In quicksort, more cores actually slowed execution with strong scaling. But, with weak scaling (each core with a fixed problem size), execution time improved.

![plot1](histstrongnooutliers.png)
![plot2](histstrongoutliers.png)
![plot3](histweaknooutliers.png)
![plot4](histweakoutliers.png)
![plot5](quickstrong.png)
![plot6](quickweak.png)

### Question 3

The macro `NWS` defines at what array size the partitioning strategy should switch from parallel to serial. For large array sizes (greater than `NWS`) it makes sense to spend time setting up the parallel overhead in order to save time populating the large arrays to the left and right of the pivot.

The macro `NTSK` defines at what array size the sorting strategy should start spawning tasks instead of serially sorting the array. When arrays start to become small relative to the original size in quicksort, the must be many sub-arrays needing to be sorted. Therefore it makes sense to assign parallel tasks to tackle the sorting of these many small arrays. For big arrays, however, there are not yet enough sub-arrays to justify parallelizing yet.

The macro `NMIN` defines at what array size the sorting strategy should stop spawning tasks and serially evaluate instead. This macro is similar to `NTSK` in that it defines bounds for when tasks should be spawned. `NMIN` ensures that the overhead associated with creating a task for essentially every array element is avoided. The combination of `NTSK` and `NMIN` makes it so only intermediate array sizes are evaluated as tasks. 

The following effects can be seen by changing the values of the three macros. Increasing `NWS` slows down execution. This makes sense because higher `NWS` means more serial partitioning. It was also found that `NWS` cannot be decreased lower than `NTSK` without throwing an error. Decreasing `NTSK` also greatly slowed execution. However, increasing `NTSK` and `NWS` together very slightly improved speed. Finally, changes to `NMIN` in either direction slowed execution speed, but the effect was much more pronounced when decreasing. This makes sense because with small `NMIN`, tons of tasks are spawned which creates a large overhead.

### Question 4

The `KMP_AFFINITY` environment variable allows a user to control how OpenMP maps threads to pysical processing units on a machine. The way it is set in `config.summit.rc`, `export KMP_AFFINITY="granularity=thread,scatter"`, tells OpenMP to scatter threads as evenly as possible throughout the system and bind to a single thread context on a single core. Depending on the system topology and algorithm structure, this can greatly impact the performance of code. For example, if nearby threads would benefit from caching the same data, a compact affinity might be preferable so that threads could be grouped on the same core and have immediate cache access. Alternatively, if threads would clash in the cache, then scattering them would be beneficial instead.