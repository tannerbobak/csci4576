/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 1 $
 * @tags $Tags: tip $
 * @date $Date: Tue Sep 26 18:22:46 2017 -0600 $
 *
 * @brief quicksort header
 */

#include <cstddef>

/* @input x the array to be sorted
 * @input n size of array x
 * @output y the output array (already allocated)
 */
void quicksort(const long *x, size_t n, long *y);

/* Header for implementation with vectors -- given in the assignment
#include <vector>

typedef std::vector<long> vec_t;

void quicksort(const vec_t &x, vec_t &y);

*/
