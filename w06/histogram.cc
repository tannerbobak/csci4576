/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision$
 * @tags $Tags$
 * @date $Date$
 */
#include "histogram.h"
#include <stdio.h>
#include "omp-utils.h"
#include <cassert>

void histogram(const vec_t &x, int num_bins,
    vec_t &bin_bdry, count_t &bin_count)
{
    // master thread
    int nt = omp_get_max_threads();
    bin_bdry.resize(num_bins+1);
    bin_count.resize(num_bins);

    //in local_bc each row is for a thread to avoid false sharing
    std::vector<count_t> local_bins(nt);

    double mn(x[0]), mx(x[0]);
    double eps(1e-14);
    #pragma omp parallel shared(mn,mx)
    {
        int t_idx = omp_get_thread_num();

        // compute min/max
        #pragma omp for reduction(min:mn) reduction(max:mx)
        for (size_t ii=0; ii < x.size(); ++ii){
            if (x[ii]<mn) mn = x[ii];
            if (x[ii]>mx) mx = x[ii];
        }

        // to make sure it is consistent among threads
        #pragma omp flush (mn,mx)
        double left (mn*(1-eps));
        double right(mx*(1+eps));
        double w((right-left)/num_bins);

        // Setting bin boundary (only needed for error checking)
        #pragma omp for
        for (size_t ii=0; ii<num_bins+1; ++ii){
            bin_bdry[ii]=left+ii*w;
        }

        // Setting the local counters to zero
        #pragma omp for
        for (int jj=0; jj<nt; ++jj) {
            local_bins[jj].resize(num_bins);
            for (size_t ii=0; ii<num_bins; ++ii)
                local_bins[jj][ii]=0;
        }

        // do the counting
        #pragma omp for
        for (size_t ii=0;ii<x.size(); ++ii){
            int idx = int((x[ii]-left)/w);
            /* error checking, can be turned off by setting NDEBUG */
            assert (idx < num_bins && idx>=0);
            assert (x[ii]>=bin_bdry[idx] && x[ii]<bin_bdry[idx+1]);
            ++local_bins[t_idx][idx];
        }

        size_t count_ii(0);
        //don't expect to gain speedup using parallel reduction
        #pragma omp for
        for (size_t ii=0; ii<num_bins; ++ii){
            count_ii = 0;
            for (int jj=0; jj<nt; ++jj)
                count_ii += local_bins[jj][ii];
            bin_count[ii] = count_ii;
        }
    }
    return;
}
