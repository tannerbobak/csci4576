# Assignment 04
Tanner Bobak

CSCI 4576

10/3/18

--- 

## Task 1

Calls to `empty.wait()`, `empty.post()`, `filled.wait()`, and `filled.post()` were added to the producer and consumer functions just before the put and get calls, respectively. That way, the producer waits for the empty semaphore and the consumer waits for the filled semaphore to have nonzero values. 

After compiling `task1.exe` and linking with `semaphore-ar.obj`, the command `./task1.exe 20 1 1 2>/dev/null |sort|uniq -c` was executed, which printed the following output:

```
      1 00001
      1 00002
      1 00003
      1 00004
      1 00005
      1 00006
      1 00007
      1 00008
      1 00009
      1 00010
      1 00011
      1 00012
      1 00013
      1 00014
      1 00015
      1 00016
      1 00017
      1 00018
      1 00019
      1 00020
     40 [Thread 0] waiting for val_lock in post
     40 [Thread 0] waiting for val_lock in wait
     15 [Thread 0] waiting for zero_lock in wait
```

This indicates that, unlike before, all numbers are being consumed and none are overwritten by producers beforehand. If there were numbers being overwritten, then not all the numbers 0-20 would be listed by this command.

## Task 2

Opted out for this assignment. Task 3 uses semaphore-ar.obj.

## Task 3

### Question 1

The variables `next_get` and `next_put` are subject to a race condition when there is more than one producer and one consumer. The semaphores make it so values in the buffer are not overwritten, but they do not prevent multiple threads from producing the same integer and placing into different spots in the buffer, nor do they prevent multiple consumers from picking up the same value in the buffer and printing it to the output. The effect of this condition is that same values may be printed more (if the value is produced multiple times or consumed multiple times) and some values may be printed fewer times (if the value is overwritten by one of the several producers).

### Question 2

Running the given shell command runs the producer-consumer code 500 times with 10 objects, 3 producer threads and 3 consumer threads. It redirects stderr to /dev/null and then sorts the standard output and lists the unique occurences of each output line. Without modifying `pc.cc`, this results in some of the numbers being printed less than then expected 500 times, and some being printed greater than the expected 500 times - a symptom of the race condition described in the previous question. 

### Question 3

To solve the above race condition, both the `put()` and `get()` calls in the producer and consumer, respectively, must be placed in OpenMP critical sections. Under the hood, critical sections implement locks for their threads. By placing these calls into critical sections, it is ensured that each value is only produced and consumed once, even though multiple threads are working on the buffer. 

With `put()` and `get()` in critical sections, each value is only printed once per iteration of `pc.exe`. 
