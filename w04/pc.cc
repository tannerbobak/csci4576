/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 11 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief
 */

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <unistd.h> // for sleep
#include "omp-utils.h"
#include "semaphore.h"

#define BSZ 5

int buffer[BSZ];
int next_put = 0;
int next_get = 0;

// Creating semaphores to communicate
Sem empty(BSZ, "empty");  // BSZ buffers are empty to begin with ...
Sem filled(0, "filled");  // ... and 0 are filled

void put(int value) {
    int idx;
    idx = next_put;
    next_put = (next_put + 1) % BSZ;
    buffer[idx] = value;
}

int get() {
    int idx;
    idx = next_get;
    next_get = (next_get + 1) % BSZ;
    return buffer[idx];
}

void* producer(int nprod, int rank) {
    TID("Starting producer");

    for (int i = 0; i < nprod; i++){
        empty.wait();
        #pragma omp critical
		{ put(nprod*rank+i+1); }
        filled.post();
    }

    TID("Producer done");
}

void* consumer(int nprod, int rank) {
    TID("Starting consumer");

    for (int i = 0; i < nprod; i++) {
        filled.wait();
		int b;
		#pragma omp critical
		{
        	b = get();
		}
        usleep(1e2); //consuming slower than producers
        printf("%05d\n",b);
		empty.post();
    }

    TID("Consumer done");
}

int main(int argc, char *argv[])
{
    if(argc < 4 ){
        printf("Need three arguements num_products, num_producer, num_consumer.\n");
        return 1;
    }
    int num_product = atoi(argv[1]);
    int np = atoi(argv[2]);
    int nc = atoi(argv[3]);
    if (omp_get_max_threads()!=np+nc) abort();

    #pragma omp parallel
    {
        #pragma omp single
        {
            for (int pIdx(0); pIdx<np; ++pIdx){
                #pragma omp task
                producer(num_product, pIdx);
            }

            for (int cIdx(0); cIdx<nc; ++cIdx){
                #pragma omp task
                consumer(num_product, cIdx);
            }
        }
    }

    return 0;
}
