/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 11 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief openmp utility functions
 */

#ifndef _OMP_UTILS_H_
#define _OMP_UTILS_H_

#include <iostream>

#ifdef _OPENMP
#include <omp.h>
#define NOW()(omp_get_wtime())
#else
#define NOW() 0
#define omp_get_thread_num() 0
#endif

#define  TID(msg) fprintf(stderr,"[Thread %d] %s\n", omp_get_thread_num(), msg)

#ifdef NDEBUG
#define LOG(msg)
#else
#define LOG(msg) printf("[Thread %d] %s\n", omp_get_thread_num(), msg)
#endif

#endif //_OMP_UTILS_H_
