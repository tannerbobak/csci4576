using MultivariateStats, Random, Printf

include("imload.jl")

#=
Create the classifier by loading images from the training data set and reducing the
dimension of the data using the PCA functionality of the MultivariateStats package.

Params: 
	path			-> Path to training data
	[sample_stride] -> Stride the data to increase speed. Default = 1
	[dims]			-> Max dims of data. Use small values to restrict. Default = 1

Returns:
	model			-> The PCA model, use to transform new points.
	labels 			-> Labels for training points.
	transform_data	-> The dimension-reduced training points.
=#
function train(path; sample_stride = 1, dims = 10)
	labels, training_data = load_all_images(path);
	#println(size(training_data))
	training_data = training_data[:,1:sample_stride:end]
	labels = labels[1:sample_stride:end]
	#println(size(training_data))
	training_data = convert(Array{Float64}, training_data)

	#= >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PARALLELIZE ME? <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< =# 
	model = MultivariateStats.fit(PCA, training_data; maxoutdim=dims)

	transform_data = MultivariateStats.transform(model, training_data)
	return model, labels, transform_data
end

#=
Get the distance between some point (that is, an image) and the traing points.

Params:
	data 	-> Trained data points with each column being a sample.
	point 	-> Point to get distances to.
Returns:
	dist 	-> Vector of distances from each point in `data` to `point`.
=#
function get_distances(data, point)
	# Distance from point to each point in data set, in order
	dist = Array{Float64}(undef, size(data,2))

	#= >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PARALLELIZE ME <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< =#
	for ii = 1 : size(data,2)
		dist[ii] = distance(data[:,ii], point)
	end
	return dist
end

#=
Calculate squared Euclidian distance between two points.

Params:
	a -> Point 1 as a vector
	b -> Point 2 as a vector
Returns:
	d -> Distance
=#
function distance(a, b)
	d = 0
	#= >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PARALLELIZE ME <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< =#
	for ii = 1 : size(a,1)
		d += (a[ii] - b[ii])^2
	end
	return d
end

#=
Classify a point using k-nearest neighbor based on data that has been dimension-
reduced using PCA.

Params:
	k 				-> Number of nearest neighbors to check.
	labels 			-> Vector of labels corresponding to transform_data
	model			-> PCA model used for transformation
	transform_data 	-> PCA-transformed training points.
	point 			-> Non-dimension-reduced point to classify as a vector
Returns:
	majority_label 	-> Label assigned to point.
=#
function classify(k, labels, model, transform_data, point)
	point = convert(Array{Float64}, point)
	transform_point = MultivariateStats.transform(model, point)
	distances = get_distances(transform_data, transform_point)

	#= >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PARALLELIZE ME? <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< =#
	distance_sorted_idx = sortperm(distances)[1:k]

	counts = Dict{Int, Int}()
	max_count = 0
	majority_label = 0
	for n in distance_sorted_idx
		label = labels[n]
		if !haskey(counts, label)
			counts[label] = 0
		end
		counts[label] += 1
		if counts[label] > max_count
			max_count = counts[label]
			majority_label = label
		end
	end
	return majority_label
end

#=
Classify all data and check accuracy.

Params:
	train_data 	-> Path to CSV of data to train on.
	test_data 	-> Path to CSV of data to test on.
Return:
	acc 		-> Accuracy
=#
function classify_all(train_data, test_data, k; arr_stride = 1, dim = 10)
	# @printf "Training...\n"
	model, labels_tr, data_tr = train(train_data; sample_stride=arr_stride, dims=dim)
	# @printf "Loading test data...\n"
	labels, data = load_all_images(test_data)
	# @printf "Classifying...\n"
	total_points = length(labels)
	correct_classifications = 0

	#= >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PARALLELIZE ME <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< =#
	for ii = 1:total_points
		#=if ii % (total_points/10) == 0
			@printf "%.0f %%\n" ii*100÷total_points
		end=#
		assignment = classify(k, labels_tr, model, data_tr, data[:,ii])
		if assignment == labels[ii]
			correct_classifications += 1
		end
	end
	acc = correct_classifications/total_points
	@printf "Classification complete. Accuracy: %.2f \n" acc
	return acc
end