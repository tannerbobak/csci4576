#=
Test serial kNN classification for training stride, dimension, and k-value
=#
using Printf, DataFrames, CSV

include("knn.jl")

let
strlist = [100,80,60,40,20,1]
dlist = [100,80,60,40,20,10,8,6,4,2]
klist = 3:4:19
checkpoint_interval = 5
interval = 0
print_header = true

test_results = DataFrame(k=Int[],Dims=Int[],Stride=Int[],Runtime=Float64[],Accuracy=Float64[])

for str in strlist
	for d in dlist
		for ki in klist
			@printf "Testing stride = %d, dims = %d, k = %d\n" str d ki
			t = @timed classify_all("fashion-mnist_train.csv", "fashion-mnist_test.csv",ki; arr_stride = str, dim=d)
			df = DataFrame(k=ki,Dims=d,Stride=str,Runtime=t[2],Accuracy=t[1])
			test_results = vcat(test_results, df)

			interval += 1
			if interval > checkpoint_interval
				println("Saving checkpoint...")
				interval = 0
				if print_header
					print_header = false
					test_results |> CSV.write("results.csv"; writeheader = true);
				else
					test_results |> CSV.write("results.csv"; append = true, writeheader = false);
				end
				test_results = DataFrame(k=Int[],Dims=Int[],Stride=Int[],Runtime=Float64[],Accuracy=Float64[])
			end

			println("===========================================")
		end
	end
end
end
