# Include me with `include("imload.jl")`

using Images, CSV, DataFrames

#= 
Load Images from Fashion MNIST dataset.

Params:
	path 	-> Path to the CSV file containing image data.
	img_dim -> Side length of an image.
Returns:
	labels	-> Type of clothing, corresponds with pages in the images array.
	images 	-> 3D matrix of size (img_dim, img_dim, length(labels)) that 
			   contains image data. Each image is on its own page in the
			   array and can be accessed with images[:,:,i] where i is the
			   image number.
=# 
function load_images(path, img_dim)

	# Load the CSV file using CSV.jl
	csv_file = CSV.File(path, header=1)

	# Parse the CSV file into a DataFrame using DataFrames.jl
	df = csv_file |> DataFrame

	# Convert the DataFrame into an array
	data_mat = convert(Array{UInt8}, df)

	# Extract the labels (the first column) from the data.
	labels = convert(Array{Int16}, data_mat[:,1])

	# Reshape the raw data and permute to get in the right format.
	# Data starts as length(labels) x img_dim^2 matrix
	# This converts it to have each image stored over dimension 1 (every column
	# is an image array)
	images = reshape(data_mat[:,2:end], (length(labels), img_dim, img_dim));
	# Swap the 1st and 3rd dimensions.
	images = permutedims(images, [3,2,1])

	# Convert values into floats.
	images = images ./ 255.

	return labels, images
end

#=
Get training data in matrix ready for PCA.

Params:
	path 	-> Path to CSV file containing image data.
	img_dim -> Side length of an image
	type 	-> Integer that corresponds to the type of image we want 
Returns:
	labels	-> Type of clothing, corresponds with columns in the images array.
	mat 	-> img_dim x samples matrix of each sample of a specific type.
			   Each column is a separate sample.
=#
function load_images_type(path, type)

	# Load the CSV file using CSV.jl
	csv_file = CSV.File(path, header=1)

	# Parse the CSV file into a DataFrame using DataFrames.jl
	df = csv_file |> DataFrame

	# Convert the DataFrame into an array
	data_mat = convert(Array{UInt8}, df)

	# Extract the labels (the first column) from the data.
	labels = convert(Array{Int16}, data_mat[:,1])

	# Extract the data from the data_mat
	data_mat = data_mat[:,2:end]

	# Extract only the data of the requested type 
	data_mat = data_mat[findall(labels .== type), :]

	# Return the transpose so individual samples are in columns, not rows.
	return labels, permutedims(data_mat, [2,1])

end

#=
Get training all data in matrix ready for PCA.

Params:
	path 	-> Path to CSV file containing image data.
	img_dim -> Side length of an image
	type 	-> Integer that corresponds to the type of image we want 
Returns:
	labels	-> Type of clothing, corresponds with columns in the images array.
	mat 	-> img_dim x samples matrix of each sample of a specific type.
			   Each column is a separate sample.
=#
function load_all_images(path)

	# Load the CSV file using CSV.jl
	csv_file = CSV.File(path, header=1)

	# Parse the CSV file into a DataFrame using DataFrames.jl
	df = csv_file |> DataFrame

	# Convert the DataFrame into an array
	data_mat = convert(Array{UInt8}, df)

	# Extract the labels (the first column) from the data.
	labels = convert(Array{Int16}, data_mat[:,1])

	# Extract the data from the data_mat
	data_mat = data_mat[:,2:end]

	# Return the transpose so individual samples are in columns, not rows.
	return labels, permutedims(data_mat, [2,1])

end