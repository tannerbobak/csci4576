#=
Test serial kNN classification for training stride, dimension, and k-value
=#
using Printf, DataFrames, CSV

include("knn-parallel.jl")

let
strlist = [100,80,60,40,20,1]
dlist = [100,80,60,40,20,10,8,6,4,2]
klist = 3:4:19
checkpoint_interval = 5
interval = 1
print_header = true

test_results = DataFrame(k=Int[],Dims=Int[],Stride=Int[],Runtime=Float64[],Accuracy=Float64[])

for str in strlist
	for d in dlist

		# @printf "Training...\n"
		model, labels_tr, data_tr = train("fashion-mnist_train.csv"; sample_stride=str, dims=d)
		# @printf "Loading test data...\n"
		labels, data = load_all_images("fashion-mnist_test.csv")
		data = convert(Array{Float64}, data)

		for ki in klist
			@printf "Testing stride = %d, dims = %d, k = %d\n" str d ki
			t = @timed classify_all(model, labels_tr, labels, data_tr, data, ki)
			df = DataFrame(k=ki,Dims=d,Stride=str,Runtime=t[2],Accuracy=t[1])
			test_results = vcat(test_results, df)

			interval += 1
			if interval > checkpoint_interval
				println("Saving checkpoint...")
				interval = 0
				if print_header
					print_header = false
					test_results |> CSV.write("results-parallel.csv"; writeheader = true);
				else
					test_results |> CSV.write("results-parallel.csv"; append = true, writeheader = false);
				end
				test_results = DataFrame(k=Int[],Dims=Int[],Stride=Int[],Runtime=Float64[],Accuracy=Float64[])
			end

			println("===========================================")
		end
	end
end
end
