using Plots, Images, ImageView

cd("/home/tanner/Documents/Boulder/Fall2018/csci4576/final")
pwd()
data_file_path = "t10k-images-idx3-ubyte"
stream = open(data_file_path, "r")
data_file_size = filesize(data_file_path)
data = Vector{UInt8}(undef, data_file_size)
readbytes!(stream, data, data_file_size)
data = data[17:end]
data_reshaped = reshape(data, (28, 28,10000));

mat = data_reshaped[:,:,1]./255
imshow(colorview(Gray, convert(Array{Float64}, mat')))